from bs4 import BeautifulSoup
from urllib.request import urlopen
import ssl

url = "https://www.qualibat.com/entreprise-correspondant-au-sirenraison-sociale/?siren_or_r_soc=paysserand+gerard"
context = ssl._create_unverified_context()
content = urlopen(url, context=context)
soup = BeautifulSoup(content, 'html.parser')
link = soup.select_one('#result-entreprise .link-td').get('href')
print(link)


content = urlopen(link, context=context)
soup = BeautifulSoup(content, 'html.parser')
print(soup.select('.right-part-entreprise .account-single .left-col .block'))
